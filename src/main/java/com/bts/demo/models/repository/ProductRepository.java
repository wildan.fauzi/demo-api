package com.bts.demo.models.repository;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bts.demo.models.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	
	@Query("Select p From Product p WHERE p.name LIKE :name")
	public List<Product> findProductByName(@PathParam("name") String name);

	@Query("SELECT p FROM Product p WHERE p.category.id = :categoryId")
	public List<Product> findProductByCategory(@PathParam("categoryId") Long categoryId);
}
