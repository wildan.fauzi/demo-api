package com.bts.demo.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bts.demo.models.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
