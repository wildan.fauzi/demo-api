package com.bts.demo.models.repository;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Product;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>, JpaSpecificationExecutor<Cart> {

	
	@Query("SELECT c FROM Cart c WHERE :product MEMBER OF c.products")
	public List<Cart> findCartByProduct(@PathParam("product") Product product);
	
	public Cart findByEmail(String email);
	
	public List<Cart> findByNameContainsIgnoreCaseOrderByIdDesc(String name);
	
	public List<Cart> findByEmailEndingWith(String prefix);
	
	public List<Cart> findByNameContainsIgnoreCaseOrEmailContainsIgnoreCase(String name, String email);
	
	public List<Cart> findAll(Specification<Cart> spec);
}
