package com.bts.demo.models.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
//@Table(name= "tbl_product")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseEntity<String> implements Serializable{
	
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="Name is required")
	private String name;
	
	@NotEmpty(message="Desc is required")
	private String description;
	
	private double price;
	
	private String image;
	
	
	@ManyToMany(mappedBy =  "products")
	@JsonBackReference
	private Set<Cart> carts = new HashSet<>();
	
	@ManyToOne
	private Category category;

	
	public Product(Long id, String name, String desc, double price) {
		super();
		this.id = id;
		this.name = name;
		this.description = desc;
		this.price = price;
	}
	
	public Product(String name, String desc, double price, Category category) {
		super();
		this.name = name;
		this.description = desc;
		this.price = price;
		this.category = category;
	}
	
	public Product(Long id, String name, String desc, double price, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.description = desc;
		this.price = price;
		this.category = category;
	}
	
	
	
}
