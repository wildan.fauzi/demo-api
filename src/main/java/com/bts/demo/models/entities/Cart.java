package com.bts.demo.models.entities;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart extends BaseEntity<String> implements Serializable{
	 
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 100, nullable = false)
	private String name;
	
	@Column(length = 200, nullable=false)
	private String address;
	
	@Column(length = 100, nullable=false, unique = true)
	private String email;
	
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(name = "cart_products", 
	joinColumns = @JoinColumn(name="cart.id"),
	inverseJoinColumns = @JoinColumn(name="product.id")
			)
//	@JsonManagedReference
	private Set<Product> products = new HashSet<>();
	
	public Cart(String name, String address, String email, Set<Product> products) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
		this.products = products;
	}
	
	public Cart(String name, String address, String email) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
	}

	
	
	
}
