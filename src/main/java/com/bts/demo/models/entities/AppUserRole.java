package com.bts.demo.models.entities;

public enum AppUserRole {
	ADMIN, USER
}
