package com.bts.demo.models.metaModels;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Category;
import com.bts.demo.models.entities.Product;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Category.class)
public abstract class Cart_ {

	public static volatile SingularAttribute<Cart, Long> id;
	public static volatile SingularAttribute<Cart, String> name;
	public static volatile SingularAttribute<Cart, String> address;
	public static volatile SingularAttribute<Cart, String> email;
	public static volatile SingularAttribute<Cart, Product> products;
	
	
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String ADDRESS = "address";
	public static final String EMAIL = "email";
	public static final String PRODUCTS = "products";
}
