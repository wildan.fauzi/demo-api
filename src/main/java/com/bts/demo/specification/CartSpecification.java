package com.bts.demo.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.metaModels.Cart_;

@Component
public class CartSpecification {
	
	public Specification<Cart> nameLike(String name){
		
		  return new Specification<Cart>() {


		@Override
		public Predicate toPredicate(Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
			 return criteriaBuilder.like(root.get(Cart_.NAME), "%"+name+"%");
		}
		  };
		}

}
