package com.bts.demo.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


@Component
public class ImageUploadHelper {
	
	private String[] listType = {"image/png", "image/jpg", "image/jpeg" };
	
	
	
	
	
	public List<String> validationImage(MultipartFile file) {
		List<String> message = new ArrayList<>();
		boolean type = false;
		
		
		if(file.isEmpty() || file == null) {
			message.add("Please select a file");
		}
		
		for (int i = 0; i < listType.length && !type; i++) {
			if(file.getContentType().equals(listType[i]) ) {
				type = true;
			}
		}
		if(!type) {
			message.add("File type is must be .png, .jpg, or .jpeg");
		}
		
		
		
		if(file.getSize() > 2000000) {
			message.add("Image size is must be less than 2 Mb");
		}
		
		return message;
	}
}
