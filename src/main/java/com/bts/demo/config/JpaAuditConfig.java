package com.bts.demo.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.bts.demo.dto.ResponseData;
import com.bts.demo.utils.AuditorAwareImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JpaAuditConfig {
	
	@Autowired
	ObjectMapper objectMapper;

	
	@Bean
	public AuditorAware<String> auditorAware(){
		return new AuditorAwareImpl();
	}
	
	@Bean
	public AccessDeniedHandler accessDeniedHandler() {
		return(request, response, ex) -> {
			
			response.setContentType("application/json");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			
			ResponseData<String> resp = new ResponseData<>();
			
			resp.setCode("401");
			resp.getMessage().add(ex.getMessage());
			resp.setStatus(false);
			
			
			;
			
			response.getOutputStream().println(objectMapper.writeValueAsString(resp));
			
		
		} ;
	}
}
