package com.bts.demo.config;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.bts.demo.dto.ResponseData;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ObjectMapper objectMapper;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
			
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		
		ResponseData<String> resp = new ResponseData<>();
		
		resp.setCode("401");
		resp.getMessage().add(authException.getMessage());
		resp.setStatus(false);
		
		
		;
		
		response.getOutputStream().println(objectMapper.writeValueAsString(resp));
//		
//		response.getOutputStream().println("{ "
//				+ "\"error\": \"" + authException.getMessage() + "\", \n"
//				+ "\"status\": false, \n"
//				+ "\"code\": 401, \n"
//				+ "}");
		
	}
	
	

}
