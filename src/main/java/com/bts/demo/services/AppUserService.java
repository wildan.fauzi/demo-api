package com.bts.demo.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bts.demo.models.entities.AppUser;
import com.bts.demo.models.repository.AppUserRepository;

@Service
@Transactional
public class AppUserService implements UserDetailsService{

	@Autowired
	AppUserRepository appUserRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return appUserRepository.findByEmail(email)
				.orElseThrow(() -> 
				new UsernameNotFoundException(
						String.format("Can't find user with email %s", email))
				);
	}
	
	public AppUser register(AppUser user) {
		boolean userExists = appUserRepository.findByEmail(user.getEmail()).isPresent();
		
		if (userExists) {
			throw new RuntimeException(
					String.format("email %s already registered", user.getEmail())		
			);
			
		};
		
		String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		
		return appUserRepository.save(user);
	}

}
