package com.bts.demo.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bts.demo.dto.User;

@Service
@Transactional
public class RestClientService {
	
	@Autowired
	RestTemplate restTemplate;

	
	public ResponseEntity<String> getUserString(int id) {
		ResponseEntity<String> response = restTemplate.getForEntity("https://jsonplaceholder.typicode.com/users/"+id, String.class);

		return response; 
	}
	
	public ResponseEntity<User> getUserObject(int id) {
		User response = restTemplate.getForObject("https://jsonplaceholder.typicode.com/users/"+id, User.class);

		return ResponseEntity.ok(response);
	}
	
	public ResponseEntity<User[]> getAll() {
		ResponseEntity<User[]> response = restTemplate.getForEntity("https://jsonplaceholder.typicode.com/users", User[].class);

		return response; 
	}
	
	public ResponseEntity<User> postUserObject(User user) {
		User response = restTemplate.postForObject("https://jsonplaceholder.typicode.com/users", user, User.class);

		return ResponseEntity.ok(response);
	}
	
	
}
