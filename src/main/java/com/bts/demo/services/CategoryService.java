package com.bts.demo.services;

import java.util.Optional;


import javax.transaction.TransactionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.bts.demo.models.entities.Category;
import com.bts.demo.models.repository.CategoryRepository;
import com.bts.demo.specification.CategorySpecification;

@Service
@TransactionScoped
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private CategorySpecification categorySpecification;
	
	@Autowired
	private TransactionTemplate transactionTemplate;
	
	@Autowired
	private PlatformTransactionManager platformTransactionManager;
	
	public Category save(Category category) {
		if(category.getId() != null) {
			Category currentCategory = categoryRepository.findById(category.getId()).get();
			currentCategory.setName(category.getName());
			
			category = currentCategory;
		}
		
	
		
		return categoryRepository.save(category);
	}
	
	public Category findOne(Long id) {
		Optional<Category> category = categoryRepository.findById(id);
		
		return !category.isPresent()? null: category.get();
	}
	
	public Iterable<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	public void removeOne(Long id) {
		transactionTemplate.execute(new TransactionCallbackWithoutResult() {
			
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				try {
					categoryRepository.deleteById(id);
					
				} catch (Exception e) {
					status.setRollbackOnly();
					e.printStackTrace();
					// TODO: handle exception
				}
				
			}
		});
	}
	
	
	
	public Iterable<Category> saveBatch(Iterable<Category> categories) {
		return categoryRepository.saveAll(categories);
	}
	
	public Iterable<Category> findByName(String name, Pageable pagable) {
		return categoryRepository.findByNameContainsIgnoreCase(name, pagable);
	}
	
	public Iterable<Category> findByName(String name) {
		return categoryRepository.findAll(categorySpecification.nameLike(name));
	}
	
	
	
	
	
	
}
