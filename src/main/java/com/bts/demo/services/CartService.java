package com.bts.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.TransactionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Product;
import com.bts.demo.models.repository.CartRepository;
import com.bts.demo.specification.CartSpecification;

@Service
@TransactionScoped
public class CartService {
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CartSpecification cartSpecification;
	

	
	public Cart save(Cart cart) {
		return cartRepository.save(cart);
	}
	
	public Cart findOne(Long id) {
		Optional<Cart> cart = cartRepository.findById(id);
		
		return !cart.isPresent()? null: cart.get();
	}
	
	public Iterable<Cart> findAll() {
		return cartRepository.findAll();
	}
	
	public void removeOne(Long id) {
		cartRepository.deleteById(id);
	}
	
//	Find Cart by Product
	
	public List<Cart> findCartByProduct(Long id) {
		Product product = productService.findOne(id);
		
		return product == null? new ArrayList<Cart>(): cartRepository.findCartByProduct(product);
		
	}
	
	public Cart findByEmail(String email) {
		return cartRepository.findByEmail(email);
	}
	
	public List<Cart> findByName(String name) {
		return cartRepository.findByNameContainsIgnoreCaseOrderByIdDesc(name);
	}
	
	public List<Cart> findByEmailEndWith(String prefix) {
		return cartRepository.findByEmailEndingWith(prefix);
	}
	
	public List<Cart> findByNameOrEmail(String name, String email) {
		return cartRepository.findByNameContainsIgnoreCaseOrEmailContainsIgnoreCase(name, email);
	}
	
	public void addProduct(Product product, Long cartId) {
		Cart cart = findOne(cartId);
		if(product == null) {
			throw new RuntimeException("Product with ID " + cartId + " not found");
		}
		
		cart.getProducts().add(product);
		save(cart);
		
	}
	
	public List<Cart> findByNameSpec(String name) {
		return cartRepository.findAll(cartSpecification.nameLike(name));
	}
	


}
