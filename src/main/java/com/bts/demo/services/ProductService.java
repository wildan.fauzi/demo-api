package com.bts.demo.services;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.bts.demo.models.entities.Cart;
import com.bts.demo.models.entities.Product;
import com.bts.demo.models.repository.ProductRepository;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Service
@Transactional
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DataSource dataSource;
	
	
	public Product save(Product product) {
		return productRepository.save(product);
	}
	
	public Product findOne(Long id) {
		Optional<Product> product = productRepository.findById(id);
		
		return !product.isPresent()? null : product.get();
		
	}
	
	@Cacheable("allProduct")
	public Iterable<Product> findAll() {
		simulationSlowProcess();
		return productRepository.findAll();
	}
	
	public void removeOne(Long id) {
		productRepository.deleteById(id);
	}
	
	public List<Product> findByName(String name) {
		return productRepository.findProductByName("%"+name+"%");
	}
	
	public List<Product> findByCategory(Long id) {
		return productRepository.findProductByCategory(id);
	}
	
	public void addCart(Cart cart, Long productId) {
		Product product = findOne(productId);
		if(product == null) {
			throw new RuntimeException("Product with ID " + productId + " not found");
		}
		
		product.getCarts().add(cart);
		save(product);
		
		
	}
	
	private Connection getConnection() {
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	public JasperPrint generateJasperPrint() throws Exception{
		InputStream fileReport = new ClassPathResource("reports/Product.jasper").getInputStream();
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, getConnection());
		
		return jasperPrint;
		
	}
	
	public JasperPrint reportByCategory(Long id) throws Exception{
		InputStream fileReport = new ClassPathResource("reports/Product_by_category.jasper").getInputStream();
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, getConnection());
		
		return jasperPrint;
			
	}
	
	
	private void simulationSlowProcess() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
}
