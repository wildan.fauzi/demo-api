package com.bts.demo.dto;

public class SearchData {

	private String searchKey;

	private String otherSearchKey;
	
	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getOtherSearchKey() {
		return otherSearchKey;
	}

	public void setOtherSearchKey(String otherSearchingKeys) {
		this.otherSearchKey = otherSearchingKeys;
	}
	
	
}
