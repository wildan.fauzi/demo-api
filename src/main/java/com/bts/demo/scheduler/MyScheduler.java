package com.bts.demo.scheduler;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bts.demo.models.entities.Category;
import com.bts.demo.models.entities.Role;
import com.bts.demo.services.AppUserService;
import com.bts.demo.services.CategoryService;
import com.bts.demo.services.RoleService;

@Component
public class MyScheduler {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	AppUserService appUserService;
	
	@Autowired
	RoleService roleService;
	
	
//	@Scheduled(fixedDelay = 5000, initialDelay = 3000)
	public void firstTask() {
		
		categoryService.save(new Category("new Category"));
		System.out.println("succesfully add new category");
		
	}
	
//	@Scheduled(cron = "0 * * * * *")
	public void secondTask() {
		roleService.save(new Role("New Role"));
		System.out.println("succesfully add new role");
	}

}
