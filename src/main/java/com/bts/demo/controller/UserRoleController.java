package com.bts.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.dto.ResponseData;
import com.bts.demo.dto.UserRoleData;
import com.bts.demo.models.entities.UserRole;
import com.bts.demo.models.entities.UserRoleId;
import com.bts.demo.services.UserRoleService;

@RestController
@RequestMapping("/api/userRole")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;

	
	@GetMapping
	public ResponseEntity<ResponseData<Iterable<UserRole>>> findAll() {
		
		ResponseData<Iterable<UserRole>> response = new ResponseData<>();
		
		response.setStatus(true);
		response.setData(userRoleService.findAll());
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseData<UserRole>> findOne(@PathVariable("id") Long id) {
		
		ResponseData<UserRole> response = new ResponseData<>();
		response.setStatus(true);
		response.setData(userRoleService.findOne(id));
		
		return ResponseEntity.ok(response);
		
	}

	
	@PostMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<UserRole>> save(@Valid @RequestBody UserRoleData userRoleData, Errors errors) {
		
		ResponseData<UserRole> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
		
		
		UserRole newUserRole = new UserRole();
		newUserRole.setUserRoleId(new UserRoleId(userRoleData.getUserId(), userRoleData.getRoleId()));
		newUserRole.setDescription(userRoleData.getDescription());
		
		if(userRoleService.save(newUserRole) == null) {
			response.setStatus(false);
			response.getMessage().add("Cant find userId or roleId");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		response.setStatus(true);
		response.setData(userRoleService.save(newUserRole));
		
		return ResponseEntity.ok(response);
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<UserRole>> update(@Valid @RequestBody UserRoleData userRoleData, Errors errors) {

		ResponseData<UserRole> response = new ResponseData<>();
		
		if(errors.hasErrors()) {
			for (ObjectError error : errors.getAllErrors()) {
				response.getMessage().add(error.getDefaultMessage());
			}
			response.setStatus(false);
			response.setData(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		
//		response.setStatus(true);
//		response.setData(userRoleService.save(userRole));
//		
//		return ResponseEntity.ok(response);
		
		UserRole newUserRole = new UserRole();
		newUserRole.setUserRoleId(new UserRoleId(userRoleData.getUserId(), userRoleData.getRoleId()));
		newUserRole.setDescription(userRoleData.getDescription());
		
		if(userRoleService.save(newUserRole) == null) {
			response.setStatus(false);
			response.getMessage().add("Cant find userId or roleId");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		response.setStatus(true);
		response.setData(userRoleService.save(newUserRole));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{userId}/{roleId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	public void removeOne(@PathVariable("userId") Long userId, @PathVariable("roleId") Long roleId) {
		
		UserRole deleted = new UserRole();
		deleted.setUserRoleId(new UserRoleId(userId, roleId));
		
		userRoleService.removeOne(deleted);
	}
}
