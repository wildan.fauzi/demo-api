package com.bts.demo.controller;

import java.util.Arrays;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.dto.CategoryData;
import com.bts.demo.dto.ResponseData;
import com.bts.demo.dto.SearchData;
import com.bts.demo.models.entities.Category;
import com.bts.demo.services.CategoryService;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Category>> save(@Valid @RequestBody CategoryData categoryData, Errors errors) {
		 ResponseData<Category> response = new ResponseData<>();
		 
		 if(errors.hasErrors()) {
				for (ObjectError error : errors.getAllErrors()) {
					response.getMessage().add(error.getDefaultMessage());
				}
				response.setStatus(false);
				response.setData(null);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			};
			
			Category category = modelMapper.map(categoryData, Category.class);
			
			response.setStatus(true);
			response.setData(categoryService.save(category));
			
			return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<ResponseData<Iterable<Category>>> findAll() {
		
		ResponseData<Iterable<Category>> response = new ResponseData<>();
		
		
		response.setStatus(true);
		response.setData(categoryService.findAll());
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ResponseData<Category>> findOne(@PathVariable("id") Long id) {
		
		ResponseData<Category> response = new ResponseData<>();
		response.setStatus(true);
		response.setData(categoryService.findOne(id));
		
		return ResponseEntity.ok(response);
		
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Category>> update(@Valid @RequestBody Category categoryData, Errors errors) {
		 ResponseData<Category> response = new ResponseData<>();
		 
		 if(errors.hasErrors()) {
				for (ObjectError error : errors.getAllErrors()) {
					response.getMessage().add(error.getDefaultMessage());
				}
				response.setStatus(false);
				response.setData(null);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			};
			
			Category category = modelMapper.map(categoryData, Category.class);
			
			response.setStatus(true);
			response.setData(categoryService.save(category));
			
			return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public void removeOne(@PathVariable("id") Long id) {
		categoryService.removeOne(id);
	}
	
	@PostMapping("/batch")
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<ResponseData<Iterable<Category>>> saveBatch(@RequestBody Category[] categories) {
		
		ResponseData<Iterable<Category>> response = new ResponseData<>();
		
		response.setData(categoryService.saveBatch(Arrays.asList(categories)));
		response.setStatus(true);
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/search/{size}/{page}")
	public ResponseEntity<ResponseData<Iterable<Category>>> findByName(@RequestBody SearchData searchData, @PathVariable("size") int size, @PathVariable int page) {
		Pageable pagable = PageRequest.of(page, size);
		ResponseData<Iterable<Category>> response = new ResponseData<>();
		
		response.setData(categoryService.findByName(searchData.getSearchKey(), pagable));
		response.setStatus(true);
		
		return ResponseEntity.ok(response);
		
	}
	
	@PostMapping("/search/{size}/{page}/{sort}")
	public  ResponseEntity<ResponseData<Iterable<Category>>> findByName(@RequestBody SearchData searchData, @PathVariable("size") int size, @PathVariable int page, @PathVariable("sort") String sort) {
		
		Pageable pagable = PageRequest.of(page, size, Sort.by("id"));
		ResponseData<Iterable<Category>> response = new ResponseData<>();
		
		if(sort.equalsIgnoreCase("desc")) {
			pagable = PageRequest.of(page, size, Sort.by("id").descending());
		}
		
		response.setData(categoryService.findByName(searchData.getSearchKey(), pagable));
		response.setStatus(true);
		
		return ResponseEntity.ok(response);
		
	}
	
	@PostMapping("/name")
	public  ResponseEntity<ResponseData<Iterable<Category>>>  findByName(@RequestBody SearchData searchData) {
		
		ResponseData<Iterable<Category>> response = new ResponseData<>();
		
		response.setData(categoryService.findByName(searchData.getSearchKey()));
		response.setStatus(true);
		
		return ResponseEntity.ok(response);
		
	}
	
	
	
}
