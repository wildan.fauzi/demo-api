package com.bts.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bts.demo.dto.User;
import com.bts.demo.services.RestClientService;


@RestController
@RequestMapping("/api/restclient")
public class RestClientController {

	@Autowired
	RestClientService service;
	
	@GetMapping("/{id}")
	public ResponseEntity<String> getUserString(@PathVariable int id) {
		
		return service.getUserString(id);
	}
	
	@GetMapping("/object/{id}")
	public ResponseEntity<User> getUserObject(@PathVariable int id) {
		
		return service.getUserObject(id);
	}
	
	@GetMapping
	public ResponseEntity<?> getAll() {
		
		return service.getAll();
	}
	
	@PostMapping
	public ResponseEntity<User> postUser(@RequestBody User user) {
		return service.postUserObject(user);
	}
	
	
	
}
