CREATE TABLE app_user (
	id  bigserial not null, 
	app_user_role varchar(255), 
	email varchar(256) not null, 
	name varchar(150) not null, 
	password varchar(256) not null, 
	primary key (id)
);

CREATE TABLE cart (
	id  bigserial not null, 
	created_by varchar(255), 
	created_date timestamp, 
	updated_by varchar(255), 
	updated_date timestamp, 
	address varchar(200) not null, 
	email varchar(100) not null, 
	name varchar(100) not null, 
	primary key (id)
);

CREATE TABLE cart_products (
	cart_id int8 not null, 
	product_id int8 not null, 
	primary key (cart_id, product_id)
);
	
CREATE TABLE category (
	id  bigserial not null, 
	created_by varchar(255), 
	created_date timestamp, 
	updated_by varchar(255), 
	updated_date timestamp, 
	deleted boolean, 
	name varchar(100) not null, 
	primary key (id)
);

CREATE TABLE product (
	id  bigserial not null, 
	created_by varchar(255), 
	created_date timestamp, 
	updated_by varchar(255), 
	updated_date timestamp, 
	description varchar(255), 
	image varchar(255), 
	name varchar(255), 
	price float8 not null, 
	category_id int8, 
	primary key (id)
	);
	
CREATE TABLE role (
	id  bigserial not null, 
	name varchar(255), 
	primary key (id)
    );
	
CREATE TABLE user_role (
	role_id int8 not null, 
	user_id int8 not null, 
	description varchar(255), 
	primary key (role_id, user_id)
	);

